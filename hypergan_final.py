from __future__ import generators, division, absolute_import, print_function
import numpy as np
import tensorflow as tf
from tensorflow.python.keras import backend as K
from tensorflow.python.ops import math_ops
import pandas as pd
import utils as ut
from sklearn.metrics import pairwise
import scipy.stats as spstats
import os

# custom constraint for signature layer, which must be non-negative and each signature must have substitution
# probabilities sum to 1


class NonNegProb(tf.keras.constraints.Constraint):
    def __init__(self, axis=0):
        self.axis = axis

    def __call__(self, w):
        h = w * math_ops.cast(math_ops.greater_equal(w, 0.), K.floatx())
        return h / math_ops.reduce_sum(h, axis=self.axis, keepdims=True)


# clipping constraint which enforces the Lipschitz condition, necessary for WGAN function


class Clipped(tf.keras.constraints.Constraint):
    def __init__(self, clip_val):
        self.clip_val = clip_val

    def __call__(self, w):
        h = tf.clip_by_value(w, -self.clip_val, self.clip_val)
        return h

# GAN class

class CANGAN:

    ## default parameters which have been determined to be useful
    ## may be still optimized using hyperopt

    def __init__(self, params={}, run_folder='C:/Users/Shawn.LAPTOP-BA3EQTSR/Downloads/Cancer_vae/summaries_wgan'):
        vals = [20, [96, 200, 100, 40, 1], [140, 500, 200, 40, 96], 0.243217388, 0.001456156, 0.9]
        keys = ['batch_size', 'disc_sizes',	'gen_sizes',  'drop_val', 'learning_rate', 'threshold']
        default_params = dict(zip(keys, vals))
        for key in default_params.keys():
            if key not in params.keys():
                params.update({key: default_params[key]})
        self.__dict__ = params
        self.run_folder_ = run_folder
        self.run_folder = run_folder

        # construct architecture and name layers for accession
        self.gen_ids = ['gen_exposures', 'gen_signatures']
        for i in range(len(self.gen_sizes[:-3])):
            self.gen_ids.insert(-2, 'gen_'+str(i+1))

        self.disc_ids = ['disc_prob']
        for j in range(len(self.disc_sizes[:-2])):
            self.disc_ids.insert(-1, 'disc_'+str(j+1))

        # reassign the latent vector since the analysis is highly sensitive to this
        self.gen_sizes[-2] = self.latent_vector

        print(self.__dict__)

    def compile(self):
        # create a new graph with a new generator and discriminator

        tf.reset_default_graph()
        self.generator = tf.keras.Sequential([
                            tf.keras.layers.Dense(units,
                                                  input_dim=self.gen_sizes[0],
                                                  name=id,
                                                  activation=tf.nn.leaky_relu)
                            for units, id in zip(self.gen_sizes[1:-2], self.gen_ids[:-2])] + [
                            tf.keras.layers.Dense(self.gen_sizes[-2],
                                                  name=self.gen_ids[-2],
                                                  activation=tf.nn.relu)], name='generator')

        # signature layer uses custome constrain and no bias
        self.signatures = tf.keras.layers.Dense(self.gen_sizes[-1],
                                                name=self.gen_ids[-1],
                                                kernel_constraint=NonNegProb(axis=1),
                                                use_bias=False)

        # in a Wasserstein GAN, the weights are clipped to within a threshold
        self.discriminator = tf.keras.Sequential([tf.keras.layers.BatchNormalization()], name='discriminator')
        for id, sz in zip(self.disc_ids[:-1], self.disc_sizes[:-1]):
            self.discriminator.add(tf.keras.layers.Dropout(rate=self.drop_val))
            self.discriminator.add(tf.keras.layers.Dense(sz,
                                                         input_dim=self.disc_sizes[0],
                                                         name=id,
                                                         activation=tf.nn.leaky_relu,
                                                         kernel_constraint=Clipped(self.clip_val),
                                                         bias_constraint=Clipped(self.clip_val)))
        self.discriminator.add(tf.keras.layers.Dense(self.disc_sizes[-1],
                                                     name=self.disc_ids[-1],
                                                     activation='sigmoid'))

    def train(self, training_matrix, label_df, n_epochs, n_bootstraps=0):

        min_kl = 10000

        for bs in range(n_bootstraps+1):

            print('bootstrap', bs)
            self.compile()

            # noise inputs: where the generator obtains noise
            noise_inputs = tf.random.normal(shape=[self.batch_size, self.gen_sizes[0]])

            # sample noise: used for analysis of energy distance
            sample_noise = tf.random.normal(shape=[1000, self.gen_sizes[0]])
            sample_gen = tf.reduce_sum(self.signatures(self.generator(sample_noise)), axis=0)

            # real data: input for batches of data to train on
            real_data = tf.placeholder(tf.float32, shape=[self.batch_size, self.disc_sizes[0]], name='real_data')

            exposures = self.generator(noise_inputs)
            fake_data = self.signatures(exposures)
            fake_prob = self.discriminator(fake_data)
            real_prob = self.discriminator(real_data)

            tf_discriminator_loss = tf.reduce_mean(fake_prob) - tf.reduce_mean(real_prob)
            tf_generator_loss = -tf.reduce_mean(fake_prob)

            #optionally feed a variable or scheduled learning rate
            lr_ph = tf.placeholder(tf.float32, shape=None, name='lr_ph')

            #RMS_Prop is REQUIRED for WGAN to function optimally
            tf_disc_loss_minimize = tf.train.RMSPropOptimizer(lr_ph,
                                                              decay=self.decay,
                                                              momentum=self.momentum,
                                                              epsilon=self.epsilon,
                                                              centered=self.centered).minimize(
                tf_discriminator_loss, var_list=self.discriminator.trainable_variables)

            tf_gen_loss_minimize = tf.train.RMSPropOptimizer(lr_ph,
                                                             decay=self.decay,
                                                             momentum=self.momentum,
                                                             epsilon=self.epsilon,
                                                             centered=self.centered).minimize(
                tf_generator_loss, var_list=self.generator.trainable_variables + self.signatures.trainable_variables)

            # scalar summaries to feed to tensorboard
            with tf.name_scope('universal'):
                gen_loss_ph = tf.placeholder(tf.float32, shape=None, name='gen_loss_ph')
                corr_ph = tf.placeholder(tf.float32, shape=None, name='correlation_ph')
                entropy_ph = tf.placeholder(tf.float32, shape=None, name='entropy_ph')

                gen_loss_summ = tf.summary.scalar('generator_loss', gen_loss_ph)
                corr_summ = tf.summary.scalar('signature_correlation', corr_ph)
                entropy_summ = tf.summary.scalar('energy_distance', entropy_ph)

                universal_summaries = tf.summary.merge([gen_loss_summ, corr_summ, entropy_summ])

            config = tf.ConfigProto(allow_soft_placement=True)
            config.gpu_options.allow_growth = True
            config.gpu_options.per_process_gpu_memory_fraction = 0.9

            session = tf.InteractiveSession(config=config)

            summ_writer = tf.summary.FileWriter(self.run_folder, session.graph)

            tf.global_variables_initializer().run()
            training_iterator = tf.data.Dataset.from_tensor_slices(
                training_matrix).batch(self.batch_size).shuffle(
                buffer_size=100, reshuffle_each_iteration=True).make_initializable_iterator()

            batch = training_iterator.get_next()

            # training loop

            for epoch in range(n_epochs):
                session.run(training_iterator.initializer)
                gl_all_epoch = []
                exp_all_epoch = []

                ITER = 0
                while True:
                    ITER += 1
                    try:
                        if ITER > 1:
                            # train the generator once
                            gl, _, exp = session.run([tf_generator_loss,
                                                 tf_gen_loss_minimize,
                                                 exposures],
                                                feed_dict={lr_ph: self.learning_rate})
                            gl_all_epoch.append(gl)
                            exp_all_epoch.append(exp)

                        for _ in range(5):
                            #train the discriminator 5 times (a hyperparameter)
                            ti = session.run(batch)

                            _ = session.run(
                                [tf_disc_loss_minimize],
                                feed_dict={real_data: ti, lr_ph: self.learning_rate})

                    except tf.errors.OutOfRangeError:
                        break

                # capture the exposures to estimate the degree to which certain sigs are actually used by the generator
                ex = np.array(exp_all_epoch).mean(axis=1)[0]
                all_sigs = np.array(self.signatures.get_weights()[0]).T #96 x latent

                # use the expected signatures from the label file to determine metrics like auc and f1
                real = list(label_df.columns)

                # de-correlate signatures: if two signatures have high cosine similarity, reset one
                # note: this means during training, there will be many transient reset signatures which are not
                # part of the GAN's solution, but are not currently filtered!
                mask = 1 - np.triu(np.ones((all_sigs.shape[1], all_sigs.shape[1])))
                corr = pairwise.cosine_similarity(all_sigs.T, all_sigs.T)
                corr *= mask
                corr_avg = corr.max()
                # the cutoff is a hyperparameter
                similar = np.argwhere(corr > self.gamma)
                for correlated in similar:
                    all_sigs[:, correlated[0]] = np.random.dirichlet(np.ones(all_sigs.shape[0]))

                self.signatures.set_weights(np.expand_dims(all_sigs.T, axis=0))

                # determine the energy distance between the mean mutation profile in the real data and the mean
                # output of the generator
                # note: this is not the kl-divergence!
                sample = session.run(sample_gen)
                sample_dist = sample/np.sum(sample)
                real_sample = np.sum(training_matrix.values, axis=0)
                real_dist = real_sample/np.sum(real_sample)
                kl_div = spstats.energy_distance(sample_dist, real_dist)

                if kl_div < min_kl:
                    min_kl = kl_div
                    pd.DataFrame(all_sigs[:, np.nonzero(ex)[0]]).to_csv(os.path.join(self.run_folder, 'extracted_signatures.csv'))
                    pd.DataFrame(ex[np.nonzero(ex)[0]]).to_csv(os.path.join(self.run_folder, 'extracted_exposures.csv'))

                gen_loss_avg = np.mean(gl_all_epoch)

                univ = session.run(universal_summaries, feed_dict={corr_ph: corr_avg,
                                                                   gen_loss_ph: gen_loss_avg,
                                                                   entropy_ph: kl_div})
                summ_writer.add_summary(univ, epoch)
            session.close()

        # after bootstrapping, go back and grab the best solution

        sigs = pd.read_csv(os.path.join(self.run_folder, 'extracted_signatures.csv'), index_col=0)
        exs = pd.read_csv(os.path.join(self.run_folder, 'extracted_exposures.csv'), index_col=0)

        # here is where low exposure signatures could be excluded
        #exs = list(exs.iloc[:, 0])
        #keep = list(np.nonzero(np.array(exs) > 0.2)[0])
        #sigs = sigs.iloc[:, keep]

        # decompose to global id signatures and save results
        sigs = sigs.values
        out = ut.compare_cosmic_gan_decomp(sigs)
        df = pd.DataFrame(out)
        df.index = ['cosine_similarity', 'fractional_weight']
        df.T.to_csv(os.path.join(self.run_folder, 'extracted_signatures.csv'))

        # calculate statistics
        out = {key: out[key][0] for key in out.keys()}
        aucroc, avp, f1 = ut.summaries(out, real, threshold=self.threshold)

        return aucroc, avp, f1, min_kl, out

    def hyperopt(self, M_list, label_list, n_epochs):
        # wraps the training function to train over multiple samples and save results and return data to hyperopt

        df = pd.DataFrame(columns=['aucroc', 'avp', 'f1', 'error', 'extracted'])

        for idx, (M, P) in enumerate(zip(M_list, label_list)):
            self.run_folder = os.path.join(self.run_folder_, 'Trial_'+str(idx))
            aucroc, avp, f1, error, extracted = self.train(M, P, n_epochs)

            df = df.append(dict(zip(['aucroc', 'avp', 'f1', 'error', 'extracted'], [aucroc, avp, f1, error, extracted])), ignore_index=True)
            print(df.drop(['extracted'], axis=1))

        df.to_csv(os.path.join(self.run_folder_, 'metrics.csv'))
        pd.DataFrame().to_csv(os.path.join(self.run_folder_, 'success.csv'))

        print(dict(df.drop(['extracted'], axis=1).mean(axis=0)))
        return dict(df.drop(['extracted'], axis=1).mean(axis=0))