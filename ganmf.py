import os
import argparse
import pandas as pd

# this script is used to run NMF on the output files from GAN, especially when GAN is accessed throught the hyperopt module
# (so that all the correct logging files are accessible)

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--run_folder', type=int)
args = parser.parse_args()

#path = '/mnt/c/Users/Shawn.LAPTOP-BA3EQTSR/Downloads/Cancer_vae/summaries_wgan/hyperopt_final_2/{}'.format(args.run_folder)
#path = '/mnt/c/Users/Shawn.LAPTOP-BA3EQTSR/Downloads/gan_real_data_2/'
path = '/mnt/c/Users/Shawn.LAPTOP-BA3EQTSR/Downloads/NMF_SIMSIGS'
os.chdir(path)
print(path)

for folder in os.listdir(path):
    inp = os.path.join(path, folder)
    out = os.path.join(path, folder)

    if not folder.__contains__('.csv') and '96' not in os.listdir(os.path.join(path, folder)): #folder not in trials:#
        os.system('python3 /mnt/c/Users/Shawn.LAPTOP-BA3EQTSR/Downloads/repos/spipe/signature_pipeline/utils/sigprofiler.py -s 2 -e 10 -n 10 -o {} -a SBS -m 96 -i {} -c 6'.format(out, inp))

df = pd.DataFrame(index=os.listdir(path), columns=['extracted_NMF', 'real', 'f1_NMF'])
for drop in ['extracted_vs_real.csv', 'metrics.csv', 'params.csv', 'success.csv', 'compare_nmf.csv']:
    try:
        df = df.drop([drop])
    except:
        continue

for folder in os.listdir(path):
    if folder.__contains__('.csv'):
        continue
    else:
        try:
            cols = pd.read_csv(os.path.join(path, folder, '96', 'Final_Solution', 'Decomposed_Solution', 'signatures.txt'), sep='\t', index_col=0).columns
            df.loc[folder, 'extracted_NMF'] = str(list(cols))
        except:
            df.loc[folder, 'extracted_NMF'] = str(list())

        cols2 = pd.read_csv(os.path.join(path, folder, 'processes.csv'), index_col=0).columns
        df.loc[folder, 'real'] = str(list(cols2))

def f1_score(x):
    y_pred = set(eval(x[0]))
    y_true = set(eval(x[1]))
    print(y_pred, y_true)
    tp = len(y_pred.intersection(y_true))
    fp = len(y_pred.difference(y_true))
    fn = len(y_true.difference(y_pred))

    if tp > 0:
        recall = tp / (tp + fn)
        precision = tp / (tp + fp)
        f1 = 2 * (precision * recall) / (precision + recall)
    else:
        f1 = 0
    return(f1)

df['f1_NMF'] = df[['extracted_NMF', 'real']].apply(lambda x: f1_score(list(x)), axis=1)

df.to_csv('extracted_vs_real.csv')

df2 = pd.read_csv(os.path.join(path, 'metrics.csv'), index_col=0)
df2.index = ['Trial_{}'.format(i) for i in list(df2.index)]

df3 = pd.concat([df, df2], axis=1)
df3.rename(columns={'f1': 'f1_GAN', 'extracted': 'extracted_GAN'}, inplace=True)
df3.drop(['aucroc', 'avp'], axis=1, inplace=True)

df3.to_csv('compare_nmf.csv')