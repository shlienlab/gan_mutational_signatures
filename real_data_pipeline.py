import os
import pandas as pd
import re
import numpy as np
import hypergan_final
from collections import Counter

os.chdir('C:/Users/Shawn.LAPTOP-BA3EQTSR/Downloads/gan_real_data_2')

truth = pd.read_csv('PCAWG_sigProfiler_SBS_signatures_in_samples.csv', index_col='Sample Names')
data = pd.read_csv('WGS_96_labeled.csv')
data.index = data['Unnamed: 0'].apply(lambda x: re.match('.*::(.*)$', x)[1])
data.index.name = 'Sample Names'
truth = truth.loc[data.index, :]
data = data.loc[truth.index, :]

truth = truth.drop(['Cancer Types', 'Accuracy'], axis=1)
data = data.drop(['Unnamed: 0', 'label', 'brca'], axis=1)

def subsample(df, fraction, output):
    trinucs_gan = pd.read_csv('C:/Users/Shawn.LAPTOP-BA3EQTSR/Downloads/Cancer_vae/subs.csv')['names']
    trinucs_nmf = ['A[C>A]A', 'A[C>A]C', 'A[C>A]G', 'A[C>A]T', 'A[C>G]A', 'A[C>G]C', 'A[C>G]G', 'A[C>G]T', 'A[C>T]A',
                   'A[C>T]C', 'A[C>T]G', 'A[C>T]T', 'A[T>A]A', 'A[T>A]C', 'A[T>A]G', 'A[T>A]T', 'A[T>C]A', 'A[T>C]C',
                   'A[T>C]G', 'A[T>C]T', 'A[T>G]A', 'A[T>G]C', 'A[T>G]G', 'A[T>G]T', 'C[C>A]A', 'C[C>A]C', 'C[C>A]G',
                   'C[C>A]T', 'C[C>G]A', 'C[C>G]C', 'C[C>G]G', 'C[C>G]T', 'C[C>T]A', 'C[C>T]C', 'C[C>T]G', 'C[C>T]T',
                   'C[T>A]A', 'C[T>A]C', 'C[T>A]G', 'C[T>A]T', 'C[T>C]A', 'C[T>C]C', 'C[T>C]G', 'C[T>C]T', 'C[T>G]A',
                   'C[T>G]C', 'C[T>G]G', 'C[T>G]T', 'G[C>A]A', 'G[C>A]C', 'G[C>A]G', 'G[C>A]T', 'G[C>G]A', 'G[C>G]C',
                   'G[C>G]G', 'G[C>G]T', 'G[C>T]A', 'G[C>T]C', 'G[C>T]G', 'G[C>T]T', 'G[T>A]A', 'G[T>A]C', 'G[T>A]G',
                   'G[T>A]T', 'G[T>C]A', 'G[T>C]C', 'G[T>C]G', 'G[T>C]T', 'G[T>G]A', 'G[T>G]C', 'G[T>G]G', 'G[T>G]T',
                   'T[C>A]A', 'T[C>A]C', 'T[C>A]G', 'T[C>A]T', 'T[C>G]A', 'T[C>G]C', 'T[C>G]G', 'T[C>G]T', 'T[C>T]A',
                   'T[C>T]C', 'T[C>T]G', 'T[C>T]T', 'T[T>A]A', 'T[T>A]C', 'T[T>A]G', 'T[T>A]T', 'T[T>C]A', 'T[T>C]C',
                   'T[T>C]G', 'T[T>C]T', 'T[T>G]A', 'T[T>G]C', 'T[T>G]G', 'T[T>G]T']
    subsampled = pd.DataFrame(index=df.index, columns=df.columns, data=np.zeros_like(df.values))
    labels = df.columns
    for row in df.iterrows():
        name = row[0]
        vals = row[1].values
        #print(vals)
        probs = vals / sum(vals)
        size = int(sum(vals)*fraction)
        choices = np.random.choice(labels, size=size, p=probs)
        c = pd.Series(dict(Counter(choices)))
        subsampled.loc[name] = c

    subsampled_gan = subsampled.loc[:, trinucs_gan]
    subsampled_gan.fillna(0).to_csv(os.path.join(output, 'subsampled_gan_{}.csv'.format(str(fraction).replace('.', ''))))
    subsampled_nmf = subsampled.loc[:, trinucs_nmf]
    subsampled_nmf.fillna(0).T.to_csv(os.path.join(output, 'subsampled_{}_nmf.SBS96.all'.format(str(fraction).replace('.', ''))), sep='\t')

    return subsampled_gan.fillna(0)

params = {'centered': True, 'clip_val': 0.005958980404642812, 'decay': 0.7633157760590673,
              'epsilon': 1.7509531302505389e-09, 'gamma': 0.9569902650548847, 'latent_vector': 20,
              'learning_rate': 0.0003314669841185401, 'momentum': 0.0012885137286119067, 'batch_size': 20}

metric = pd.DataFrame(index=['Trial_{}'.format(x) for x in range(100)], columns=['aucroc', 'sigs', 'real', 'f1_0.8'])
for i in range(100):
    path = os.path.join(os.getcwd(), 'Trial_{}'.format(i))
    if not os.path.exists(path):
        os.makedirs(path)
    samples = np.random.choice(truth.index, 100, replace=False)
    sample_truth = truth.loc[samples, :]
    sample_data = data.loc[samples, :]
    labels = sample_truth.replace(0, np.nan).count()
    labels = sample_truth.columns[(labels > 5)]
    labels = pd.DataFrame(data=labels, index=labels).T
    subsampled = subsample(sample_data, 0.001, str(i), 'Trial_{}'.format(i))
    gan = hypergan_final.CANGAN(params, run_folder=os.path.join(os.getcwd(), 'Trial_{}'.format(i)))
    auc, avp, f1, kl, sigs = gan.train(subsampled, labels, 1000, 0)
    metric.loc['Trial_{}'.format(i), :] = auc, sigs, labels.values, f1
    print(metric.head())
    print(i, auc, avp, f1, kl)

metric.to_csv('metrics.csv')
