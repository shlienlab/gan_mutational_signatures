import os
import pandas as pd
import hypergan_final as hypergan
import utils as ut
import numpy as np
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials, partial
import time
import csv

path = os.path.join('C:/Users/Shawn.LAPTOP-BA3EQTSR/Downloads/Cancer_vae/summaries_wgan/hyperopt_final_2')
if not os.path.exists(path):
    os.makedirs(path)

#training epochs
n_epochs = 1000

def objective(params):
    global ITERATION
    ITERATION += 1

    params['decay'] = 1-params['decay']
    params['gamma'] = 1-params['gamma']

    path2 = os.path.join(path, str(ITERATION))
    while os.path.exists(path2):
        ITERATION += 1
        path2 = os.path.join(path, str(ITERATION))
    os.makedirs(path2)

    pd.DataFrame({key: str(val) for (key, val) in params.items()}, index=params.keys()).to_csv(os.path.join(path2, 'params.csv'))
    M_list, P_list, _ = ut.create_mutsigs_hyper(0, 8, 3, 2, 4, (25, 50), 100, 0.15, path2, 100)

    # force  the GAN to use certain parameters (overriding the ones supplied by hyperopt)
    params = {'centered': True, 'clip_val': 0.00589742477365244, 'decay': 0.907365069216517, 'epsilon': 3.4134271168371417e-12,
     'gamma': 0.9467192419675178, 'latent_vector': 74.0, 'learning_rate': 0.0009329910081302326,
     'momentum': 0.07728494607914463, 'batch_size': 20}

    gan = hypergan.CANGAN(params, run_folder=path2)
    t0 = time.clock()
    roc, avp, f1, error = gan.hyperopt(M_list, P_list, n_epochs).values()
    t = time.clock()-t0
    of_connection = open(os.path.join(path, 'results.csv'), 'a')
    writer = csv.writer(of_connection)
    writer.writerow([ITERATION, roc, avp, f1, error, params, t])

    return {'loss': 1-avp, 'params': params, 'time': t, 'status': STATUS_OK, 'iteration': ITERATION}


# search space for hyperparameters
space = {'learning_rate': hp.normal('learning_rate', 0.001, 2e-4),
         'latent_vector': hp.qloguniform('latent_vector', np.log(4), np.log(200), 1),
         'decay': hp.loguniform('decay', np.log(0.01), np.log(0.5)),
         'momentum': hp.choice('momentum', [hp.loguniform('momentum_1', np.log(0.001), np.log(1)), 0]),
         'epsilon': hp.lognormal('epsilon', np.log(1e-10), 4),
         'centered': hp.choice('centered', [True, False]),
         'clip_val': hp.lognormal('clip_val', np.log(0.01), 1),
         'gamma': hp.loguniform('gamma', np.log(0.01), np.log(0.5))}

con = open(os.path.join(path, 'results.csv'), 'a')
writer = csv.writer(con)
writer.writerow(['iteration', 'aucroc', 'precision-avg', 'f1_0.9', 'recon_error', 'params', 'time'])
con.close()

# hyperopt uses this object to save process data
gan_trials = Trials()

global ITERATION
ITERATION = 0

best = fmin(fn=objective,
            space=space,
            algo=partial(tpe.suggest, n_startup_jobs=20),
            max_evals=2000,
            trials=gan_trials,
            rstate=np.random.RandomState(21),
            verbose=1)











