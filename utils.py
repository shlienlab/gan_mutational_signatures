from sklearn.metrics import pairwise as pw
import sys
sys.path.append('C:/Users/Shawn.LAPTOP-BA3EQTSR/Downloads/repos/signature_pipeline/utils/sigproextractor')
import numpy as np
import pandas as pd
import os
import re
import subroutines
from sklearn.metrics import average_precision_score, roc_auc_score
from scipy.optimize import nnls

d = 'C:/Users/Shawn.LAPTOP-BA3EQTSR/Downloads/Cancer_vae'
subs_signatures_file = os.path.join(d, 'references/sigProfiler_SBS_signatures_2018_03_28.csv')

subs_signatures = pd.read_csv(subs_signatures_file, sep=',')
subs_signatures['names'] = [
    subs_signatures['Type'][i] + subs_signatures['SubType'][i] for i in range(subs_signatures.shape[0])]
subs_signatures = subs_signatures.set_index('names')
subs_signatures = subs_signatures.drop(['Type', 'SubType'], axis=1)


trinuc_cols = ['A[C>A]A', 'A[C>A]C', 'A[C>A]G', 'A[C>A]T', 'A[C>G]A', 'A[C>G]C', 'A[C>G]G', 'A[C>G]T', 'A[C>T]A',
                 'A[C>T]C', 'A[C>T]G', 'A[C>T]T', 'A[T>A]A', 'A[T>A]C', 'A[T>A]G', 'A[T>A]T', 'A[T>C]A', 'A[T>C]C',
                 'A[T>C]G', 'A[T>C]T', 'A[T>G]A', 'A[T>G]C', 'A[T>G]G', 'A[T>G]T', 'C[C>A]A', 'C[C>A]C', 'C[C>A]G',
                 'C[C>A]T', 'C[C>G]A', 'C[C>G]C', 'C[C>G]G', 'C[C>G]T', 'C[C>T]A', 'C[C>T]C', 'C[C>T]G', 'C[C>T]T',
                 'C[T>A]A', 'C[T>A]C', 'C[T>A]G', 'C[T>A]T', 'C[T>C]A', 'C[T>C]C', 'C[T>C]G', 'C[T>C]T', 'C[T>G]A',
                 'C[T>G]C', 'C[T>G]G', 'C[T>G]T', 'G[C>A]A', 'G[C>A]C', 'G[C>A]G', 'G[C>A]T', 'G[C>G]A', 'G[C>G]C',
                 'G[C>G]G', 'G[C>G]T', 'G[C>T]A', 'G[C>T]C', 'G[C>T]G', 'G[C>T]T', 'G[T>A]A', 'G[T>A]C', 'G[T>A]G',
                 'G[T>A]T', 'G[T>C]A', 'G[T>C]C', 'G[T>C]G', 'G[T>C]T', 'G[T>G]A', 'G[T>G]C', 'G[T>G]G', 'G[T>G]T',
                 'T[C>A]A', 'T[C>A]C', 'T[C>A]G', 'T[C>A]T', 'T[C>G]A', 'T[C>G]C', 'T[C>G]G', 'T[C>G]T', 'T[C>T]A',
                 'T[C>T]C', 'T[C>T]G', 'T[C>T]T', 'T[T>A]A', 'T[T>A]C', 'T[T>A]G', 'T[T>A]T', 'T[T>C]A', 'T[T>C]C',
                 'T[T>C]G', 'T[T>C]T', 'T[T>G]A', 'T[T>G]C', 'T[T>G]G', 'T[T>G]T']

processes = list(pd.read_csv('C:\\Users\\Shawn.LAPTOP-BA3EQTSR\\Downloads\\Cancer_vae\\summaries_wgan\\hyperopt_final\\265\\Trial_1\\processes.csv')['names'])


## USED FOR NMF TESTING

def create_mutsigs(name, folder):
    # parse the name passed by another program to create a file
    #n_sigs_3_each_2-3_bounds_50-100_samples_100_noise_025_req_sigs_[35]
    n_sigs = int(re.match('.*n_sigs_(\d+)_.*', name)[1])
    min_sigs = int(re.match('.*each_(\d+)-(\d+)_.*', name)[1])
    max_sigs = int(re.match('.*each_(\d+)-(\d+)_.*', name)[2])
    bounds_ = re.match('.*bounds_(\d+-\d+)_.*', name)[1]
    n_samples = int(re.match('.*samples_(\d+)_.*', name)[1])
    noise = re.match('.*noise_(\d+)_.*', name)[1]
    try:
        req_sigs_raw = re.match('.*req_sigs_\[(.*)\].*', name)[1]
        req_sigs = ['SBS' + req_sigs_raw]
    except:
        req_sigs = []

    bounds_ = re.sub('-', ', ', bounds_)
    bounds = eval(bounds_)

    if '0' in noise:
        dec = noise.index('0') + 1
        noise = noise[:dec] + '.' + noise[dec:]
    noise = float(noise)

    metadata = {'n_sigs': n_sigs, 'bounds': bounds_, 'min_sigs': min_sigs, 'max_sigs': max_sigs, 'n_samples': n_samples,
                'noise': noise}

    '''
    Using between (n_min, n_max) signatures from cosmic, construct a (n_samples) row dataframe which is a
    simulation of a mutational catalog for (n_samples) patients. In each row, add (total_muts) mutations
    which is a range representing a patient's total mutational burden. Then optionally add Poisson noise.
    Label each row with the signatures and exposures used to create it if (label_sigs).
    '''

    sig_names = ['SBS1', 'SBS2', 'SBS3', 'SBS4', 'SBS5', 'SBS6', 'SBS7a', 'SBS7b',
                 'SBS7c', 'SBS7d', 'SBS8', 'SBS9', 'SBS10a', 'SBS10b', 'SBS11', 'SBS12',
                 'SBS13', 'SBS14', 'SBS15', 'SBS16', 'SBS17a', 'SBS17b', 'SBS18',
                 'SBS19', 'SBS20', 'SBS21', 'SBS22', 'SBS23', 'SBS24', 'SBS25', 'SBS26',
                 'SBS27', 'SBS28', 'SBS29', 'SBS30', 'SBS31', 'SBS32', 'SBS33', 'SBS34',
                 'SBS35', 'SBS36', 'SBS37', 'SBS38', 'SBS39', 'SBS40', 'SBS41', 'SBS42',
                 'SBS43', 'SBS44', 'SBS45', 'SBS46', 'SBS47', 'SBS48', 'SBS49', 'SBS50',
                 'SBS51', 'SBS52', 'SBS53', 'SBS54', 'SBS55', 'SBS56', 'SBS57', 'SBS58',
                 'SBS59', 'SBS60']

    # choose the signatures for the cohort, starting by including the required sigs
    sel_sigs = set(req_sigs)
    while len(sel_sigs) < n_sigs:
        sel_sigs.add(np.random.choice(sig_names, replace=False))
    cosmic_df = subs_signatures[sel_sigs]
    metadata.update({'simulated_signatures': sel_sigs})

    # put the cosmic index in the same form
    c_index = list(cosmic_df.index)
    new_index = []
    for i in c_index:
        sub = i[0:3]
        sub2 = '[{}]'.format(sub)
        new_index.append(i[3] + sub2 + i[5])

    n_muts = cosmic_df.shape[0]  # 96 trinuc context, 64 signatures
    all_vars = np.zeros([n_samples, n_muts])  # output df structure
    all_sigs = cosmic_df.columns  # index for 64 signatures

    labels = list(np.zeros(n_samples))  # information about which signatures are incorporated, including exposures

    scount = 0
    for i in range(n_samples):
        m = np.random.randint(low=bounds[0], high=bounds[1]+1)  # pick a mutation number (burden) in the specified range
        n_rsigs = np.random.randint(min_sigs, max_sigs+1)  # number of signatures to combine
        rand_sigs = np.random.choice(all_sigs, n_rsigs, replace=False)  # which sigs to choose, excluding labelled
        probs = np.random.dirichlet(np.ones(n_rsigs))
        probs = np.round(probs * m) #exposures
        reconst = np.zeros(n_muts)
        for j in range(n_rsigs):
            sig = cosmic_df[rand_sigs[j]]
            exp = probs[j]
            reconst += np.asarray(sig * exp)
        labels[scount] = dict(zip(rand_sigs, probs))
        # noise used in previous analyses is commented out
        #noise_mask = np.random.poisson(reconst * noise)
        noise_mask = np.random.poisson(noise, size=reconst.shape)
        reconst = reconst + noise_mask
        all_vars[scount, :] = np.round(reconst)
        scount += 1

    all_vars_df = pd.DataFrame(
        all_vars.T, columns=['Sample {}'.format(i + 1) for i in range(n_samples)], index=new_index)
    all_vars_df = all_vars_df.T

    labelled_df = pd.DataFrame(labels)

    all_vars_loc = os.path.join(folder, name + '.SBS96.all')
    all_vars_loc2 = os.path.join(folder, 'mutation_profile.csv')
    all_vars_df.to_csv(all_vars_loc, sep='\t')
    all_vars_df.to_csv(all_vars_loc2)

    # labelled_loc = os.path.join(folder, name + '_labels.csv')
    labelled_loc2 = os.path.join(folder, 'exposures.csv')
    labelled_df[np.isnan(labelled_df)] = 0
    # labelled_df.to_csv(labelled_loc)#, sep='\t')
    labelled_df.to_csv(labelled_loc2)

    cosmic_df.to_csv(os.path.join(folder, 'processes.csv'))

    # pd.Series(metadata).to_csv(os.path.join(folder, 'simulation_metadata.csv'), header=False)

    return all_vars_df, labelled_df, cosmic_df, metadata

# USED TO CREATE SIGNATURES FOR GAN

def create_mutsigs_hyper(bg_sigs_, target_sigs_, n_nsigs, min_sigs, max_sigs, bounds, n_samples, weight, folder, n_trials):

    M_list = []
    P_list = []
    E_list = []

    for trial in range(n_trials):

        bg_sig_names = ['SBS1', 'SBS4', 'SBS5',  'SBS7a', 'SBS7b',
                     'SBS7c', 'SBS7d', 'SBS8', 'SBS9', 'SBS12',
                     'SBS14',  'SBS16', 'SBS17a', 'SBS17b',
                     'SBS19',  'SBS21', 'SBS22', 'SBS23', 'SBS24', 'SBS25', 'SBS26',
                     'SBS27', 'SBS28', 'SBS29',  'SBS32', 'SBS33', 'SBS34',
                     'SBS37', 'SBS38', 'SBS39', 'SBS40', 'SBS41', 'SBS42',
                     'SBS43',  'SBS45', 'SBS46', 'SBS47', 'SBS48', 'SBS49', 'SBS50',
                     'SBS51', 'SBS52', 'SBS53', 'SBS54', 'SBS55', 'SBS56', 'SBS57', 'SBS58',
                     'SBS59', 'SBS60']

        bg_sigs = list(np.random.choice(bg_sig_names, bg_sigs_, replace=False))

        target_sig_names = ['SBS2', 'SBS3', 'SBS6', 'SBS10a', 'SBS10b', 'SBS11', 'SBS13', 'SBS15', 'SBS18',
                            'SBS20', 'SBS30', 'SBS31', 'SBS35', 'SBS36', 'SBS44']

        target_sigs = list(np.random.choice(target_sig_names, target_sigs_, replace=False))

        metadata = {'background_sigs': bg_sigs, 'target_sigs': target_sigs, 'bounds': bounds, 'min_sample_sigs': min_sigs,
                    'max_sample_sigs': max_sigs, 'n_samples': n_samples, 'weight_of_added': weight}

        P = subs_signatures[target_sigs+bg_sigs]

        def reformat(i):
            sub = i[0:3]
            sub2 = '[{}]'.format(sub)
            return i[3] + sub2 + i[5]

        new_index = pd.Series(P.index).apply(lambda x: reformat(x)).values

        n_muts = len(new_index)  # 96 trinuc context, 64 signatures
        M = np.zeros([n_samples, n_muts])  # output df structure
        E = list(np.zeros(n_samples))  # information about which signatures are incorporated, including exposures

        scount = 0
        for i in range(n_samples):
            noise_sigs = np.random.choice(bg_sig_names, n_nsigs)
            while any(b == n for b in bg_sigs for n in noise_sigs):
                noise_sigs = np.random.choice(bg_sig_names, n_nsigs)

            total_exp = np.random.randint(low=bounds[0], high=bounds[1]+1)
            texp = np.random.randint(low=0, high=total_exp+1)  # pick a mutation number (burden) in the specified range
            bgexp = total_exp - texp
            nexp = np.random.randint(low=round(bounds[0]*weight), high=round(bounds[1]*weight))

            total_sigs = np.random.randint(min_sigs, max_sigs+1)
            n_tsigs = 0
            n_bgsigs = 0

            while n_tsigs+n_bgsigs != total_sigs:
                n_tsigs = int(np.random.randint(0, target_sigs_+1)) # number of signatures to combine
                n_bgsigs = int(np.random.randint(0, bg_sigs_+1))

            r_tsigs = list(np.random.choice(target_sigs, n_tsigs, replace=False)) # which sigs to choose, excluding labelled
            r_bgsigs = list(np.random.choice(bg_sigs, n_bgsigs, replace=False))
            r_nsigs = noise_sigs

            probs = np.random.dirichlet(np.ones(total_sigs))
            t_probs = list(np.floor(probs[:n_tsigs] * texp)) # distribute the mutation burden over the target signatures
            bg_probs = list(np.floor(probs[n_tsigs:] * bgexp))
            n_probs = np.round(np.random.dirichlet(np.ones(n_nsigs)) * nexp)

            reconst = np.zeros(n_muts)
            for j in range(n_tsigs):
                sig = subs_signatures[r_tsigs[j]]
                exp = t_probs[j]
                reconst += np.asarray(sig * exp)
            for k in range(n_bgsigs):
                sig = subs_signatures[r_bgsigs[k]]
                exp = bg_probs[k]
                reconst += np.asarray(sig, exp)
            for l in range(n_nsigs):
                sig = subs_signatures[r_nsigs[l]]
                exp = n_probs[l]
                noise = np.random.poisson(lam=0.05, size=reconst.shape)
                reconst += np.asarray(sig * exp) + noise

            E[scount] = dict(zip(r_tsigs+r_bgsigs, t_probs+bg_probs))
            M[scount, :] = np.round(reconst)
            scount += 1

        M = pd.DataFrame(
            M.T, columns=['Sample {}'.format(i + 1) for i in range(n_samples)], index=new_index)
        M = M.T#[trinuc_cols]

        P.index = pd.Series(P.index).apply(lambda x: reformat(x))
        #P = P.loc[trinuc_cols, :]

        E = pd.DataFrame(E)

        E[np.isnan(E)] = 0
        if not os.path.exists(os.path.join(folder, 'Trial_{}'.format(trial))):
            os.makedirs(os.path.join(folder, 'Trial_{}'.format(trial)))
        labelled_loc = os.path.join(folder, 'Trial_{}'.format(trial), 'exposures.csv')
        E.to_csv(labelled_loc)

        all_vars_loc = os.path.join(folder, 'Trial_{}'.format(trial), '{}_nmf.SBS96.all'.format(trial))
        all_vars_loc2 = os.path.join(folder, 'Trial_{}'.format(trial), 'mutation_profile.csv')
        M[trinuc_cols].T.to_csv(all_vars_loc, sep='\t')
        M.to_csv(all_vars_loc2)

        P.to_csv(os.path.join(folder, 'Trial_{}'.format(trial), 'processes.csv'))

        pd.Series(metadata).to_csv(os.path.join(folder, 'Trial_{}'.format(trial), 'simulation_metadata.csv'), header=False)

        M_list.append(M)
        P_list.append(P)
        E_list.append(E)

    return(M_list, P_list, E_list)


def recon(sigs, profile, raw=None):
    # used to determine the reconstruction error of the signature matrix
    if raw is not None:
        A = raw
    else:
        A = subs_signatures[sigs]
    B = profile
    total_err = 0
    exposures = np.zeros((B.shape[0], A.shape[1]))
    for i in range(B.shape[0]):
        b = B[i, :]
        try:
            exp, err = nnls(A, b)
        except ValueError:
            exp, err = None, np.nan
        exposures[i, :] = exp
        total_err += err

    return exposures, total_err


def compare_cosmic_gan_decomp(signatures):

    # this is the global id signature decomposition script from sigprofiler, just ported

    sigDatabase = pd.read_csv(subs_signatures_file, sep=',')
    sigDatabase = sigDatabase.sort_values(['SubType'], ascending=[True])
    List = list("A" * 24) + list("C" * 24) + list("G" * 24) + list("T" * 24)
    sigDatabase['group'] = List
    sigDatabase = sigDatabase.sort_values(['group', 'Type'], ascending=[True, True]).groupby('group').head(96)
    new_index = sigDatabase.apply(lambda x: x['SubType'][0]+'[{}]'.format(x['Type'])+x['SubType'][2], axis=1)
    sigDatabase = sigDatabase.iloc[:,2:-1]
    signames = sigDatabase.columns

    signatures = pd.DataFrame(signatures, index=processes)
    signatures = signatures.loc[new_index, :].values

    # replace the probability data of the process matrix with the number of mutation
    for i in range(signatures.shape[1]):
        signatures[:, i] = signatures[:, i] * 5000  # (np.sum(exposureAvg[i, :]))

    sigDatabase = np.array(sigDatabase)

    output_dict = {}
    for i in range(signatures.shape[1]):
        exposures, similarity = subroutines.add_signatures(sigDatabase, signatures[:, i][:, np.newaxis])

        # 4 would be an import hyperparameter to change
        if len(np.nonzero(exposures)[0]) > 4:
            continue
        exposure_percentages = exposures[np.nonzero(exposures)] / np.sum(exposures[np.nonzero(exposures)])

        for num, j in enumerate(np.nonzero(exposures)[0]):
            if signames[j] in output_dict.keys():
                if output_dict[signames[j]][0] < similarity:
                    output_dict[signames[j]] = (similarity, exposure_percentages[num])
            else:
                output_dict[signames[j]] = (similarity, exposure_percentages[num])

    return output_dict


def summaries(extracted, real, threshold=0.9):

    classes = ['SBS1', 'SBS2', 'SBS3', 'SBS4', 'SBS5', 'SBS6', 'SBS7a', 'SBS7b',
               'SBS7c', 'SBS7d', 'SBS8', 'SBS9', 'SBS10a', 'SBS10b', 'SBS11', 'SBS12',
               'SBS13', 'SBS14', 'SBS15', 'SBS16', 'SBS17a', 'SBS17b', 'SBS18',
               'SBS19', 'SBS20', 'SBS21', 'SBS22', 'SBS23', 'SBS24', 'SBS25', 'SBS26',
               'SBS27', 'SBS28', 'SBS29', 'SBS30', 'SBS31', 'SBS32', 'SBS33', 'SBS34',
               'SBS35', 'SBS36', 'SBS37', 'SBS38', 'SBS39', 'SBS40', 'SBS41', 'SBS42',
               'SBS43', 'SBS44', 'SBS45', 'SBS46', 'SBS47', 'SBS48', 'SBS49', 'SBS50',
               'SBS51', 'SBS52', 'SBS53', 'SBS54', 'SBS55', 'SBS56', 'SBS57', 'SBS58',
               'SBS59', 'SBS60']

    y_true = [int(c in real) for c in classes]
    y_score = [(extracted[key] if key in extracted else 0) for key in classes]
    aucroc = roc_auc_score(y_true, y_score)
    avp = average_precision_score(y_true, y_score)

    y_pred = np.array([int(s > threshold) for s in y_score])
    y_true = np.array(y_true)
    if any(y_pred):
        tp = sum(np.logical_and(y_pred, y_true))
        fp = sum(np.logical_and(y_pred, np.logical_not(y_true)))
        fn = sum(np.logical_and(y_true, np.logical_not(y_pred)))
        precision = tp / (tp + fp)
        recall = tp / (tp + fn)
        if tp > 0:
           f1 = 2 * (precision * recall) / (precision + recall)
        else:
           f1 = 0
    else:
        precision, recall, f1 = 0, 0, 0

    return aucroc, avp, f1